# Erlang

This is my learning experience of Erlang.

## Built-in Functions

- `is_tuple(T)`
- `tuple_size(T)`
- `element(N, T)` - the Nth element of tuple `T`
  - 1-indexed, _NOT_ 0-indexed
- `hd(L)` - head of list `L`
-
