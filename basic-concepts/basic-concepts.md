# Basics

## Erlang Shell

- Use `q().` to gracefully exit the Erlang shell
  - _all_ open files are flushed
  - DBs are stopped
  - _all_ applications closed in order
  - shell alias for `init:stop()`
- **Annotations** _CANNOT_ be entered in shell
  - `-module`
  - `-export`
- You _can_ start/stop multiple shells
- `f().` - _forget_ any/all bindings!

## Arithmetic

- Arbitrary-sized integers are used for performing integer arithmetic
- Integer arithmetic is _exact_

## Variables

- `=` - **pattern matching operator**
- **Single-assignment** variables
- the _scope_ of a variable is the _lexical unit_ in which it's defined
- Variables acquire values as the result of a successful pattern matching operations
  - `Lhs = Rhs.`
    - evaluate `Rhs`
    - match the result against the pattern on the left side `Lhs`
- You _can_ `Lhs = Rhs.` later _only if_ `Rhs` is identical
- Erlang has **immutable state**

## Floating-Point

- When two integers are divided using `/`, the result is _automatically_ converted to a floating-point number
  - Obtain an integer result:
    - `div`
    - `rem`
- 64-bit floats

## Atoms

- Represent _constant_ values
- Atoms are _global_
- the value of an atom is _just the atom_

## Tuples

- Fixed number of items
- `{,}`
- Fields have _no_ names
- _Recommended_:
  - Use an _atom_ as the first element of the tuple
  - `{point, 1.0, 2.0}` to represent the "field name"
- _Can_ be nested
- Use `=` to extract values from a tuple
- Use `_` for variables that are not of interest
  - _anonymous variable_

## Lists

- individual elements can be of any type
- **head** of the list - first element of a list
  - everything else is **tail**
- Head can be anything, but tail is usually also a list
- `[H|T]` - a list with head `H` and tail `T`, where `T` is a list itself
- _Make sure_ `T` is a list when doing `[...|T]`!
- Use pattern matching to extract elements from a list!
  - `[Head, Var1, ...|Tail] = List.`

## Strings

- Strictly speaking, there are _NO_ strings, only:
  - a list of integers (where each element represents a Unicode codepoint), or
  - a binary
- _string literal_ - a sequence of chars enclosed in _double_ quotes (`""`)
- When shell prints a list, it prints as a string literal _if_ _all_ integers in the list represent printable characters; otherwise it prints in list notation
- Use `$` to represent the integer value that represents a char
  - `I = $a.`
- To force to print a list of integers, use `~w`:
  - `io:format("~w~n", ["abs"]).`
-
