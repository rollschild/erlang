-module(afile_server).

-export([start/1, loop/1]).

start(Dir) ->
    spawn(afile_server, loop, [Dir]).

% Dir: current working directory of the file server
% loop is a function that never returns
loop(Dir) ->
    receive
        % Client becomes *bound* as part of the pattern matching process
        % Client is the PID of the process that sent the request
        % server should send the reply to Client
        % self() is the PID of server
        % Two patterns here:
        {Client, list_dir} ->
            Client ! {self(), file:list_dir(Dir)};
        {Client, {get_file, File}} ->
            Full = filename:join(Dir, File),
            Client ! {self(), file:read_file(Full)}
    end,
    % infinite loop
    loop(Dir).
