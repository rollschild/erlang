# Intro

## Concurrency

- We _can_ run a concurrent program on a single-core CPU, but _cannot_ run a parallel program on it
- `module(module-name).` - defines a module
  - the module name is an **atom**
- `spawn`
  - an Erlang primitive that creates a concurrent process and returns a process identifier (PID)
  - _not_ an OS process, but a process managed by Erlang
- Processes share _NO_ memory and can only communicate with each other by sending messages
- An Erlang process is a little virtual machine that can evaluate a single Erlang function
- `Pid ! Msg`
  - its return value is _defined_ to be `Msg`
  -
- The keys to fault tolerance are:
  - independence, and
  - hardware redundancy
- _CANNOT_ rebind variables!
- `=` is a **pattern matching operator**
  - _NOT_ an assignment operator
- Erlang variables start with uppercase characters!
- Names beginning with lowercase letters are symbolic constants - **atom**s
- Modules end with `.erl` and must be compiled before run

## Processes, Modules, and Compilation

- `c` - the compile command in the `erl` shell
- `erlc` - the compile command CLI
  - _preferred_
- `erl`
  - `erl -noshell -s hello start -s init stop`
    - loads the module and evaluates
    - then evaluates `init:stop()`, which terminates the Erlang session
- Erlang has **tail-call optimization**
  - infinite loops will actually run in constant space
- Pattern matching:

```erlang
receive
    Pattern1 ->
      Actions1;
    Pattern2 ->
      Actions2;
    ...
end
```

-
