-module(funs).

-export([filter_list/1]).

% You _CANNOT_ declare a fun and assign it to a variable at the top level of a module
% You have to do it inside a function
filter_list(L) ->
    Even = fun(X) -> X rem 2 =:= 0 end,
    lists:filter(Even, L).
