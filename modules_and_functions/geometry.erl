% module declaration
-module(geometry).

% N, in this case `1`, is *arity*
-export([test/0, area/1]).

% head and body, separated by ->
area({rectangle, Width, Height}) ->
    Width * Height;
area({square, Side}) ->
    Side * Side;
area({circle, Radius}) ->
    3.1415926 * Radius * Radius.

test() ->
    % this is a test
    12 = area({rectangle, 3, 4}),
    144 = area({square, 12}),
    tests_passed.
