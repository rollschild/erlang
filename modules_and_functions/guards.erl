-module(guards).

-export([test_func/2]).

test_func(X, Y) when is_integer(X), X > Y; abs(Y) < 23 ->
    true;
test_func(X, Y) ->
    false.
