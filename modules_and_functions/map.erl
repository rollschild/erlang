-module(map).

-export([map/2, pmap/2]).

map(F, [H | T]) ->
    [F(H) | map(F, T)];
map(_, []) ->
    [].

pmap(F, L) ->
    S = self(),
    %% make_ref() returns a _unique_ reference
    Ref = erlang:make_ref(),
    Pids = map(fun(I) -> spawn(fun() -> do_f(S, Ref, F, I) end) end, L),
    %% gather the results
    gather(Pids, Ref).

do_f(Parent, Ref, F, I) ->
    Parent ! {self(), Ref, catch F(I)}.

%% **selective receive**
gather([Pid | T], Ref) ->
    receive
        {Pid, Ref, Ret} ->
            [Ret | gather(T, Ref)]
    end;
gather([], _) ->
    [].
