# Moduels and Functions

## Modules

- compiled modules have the extension `beam`

## Functions

- Expressions in the function body are evaluated _if_ the pattern in the head is successfully matched against the calling arguments
- Function **clause**s are separated by `;`
- return value of the function is the value of the _last_ expression in the body of the _clause_
- In general, clause order _does_ matter, unless:
  - Patterns in the clause are _mutually exclusive_
- When no pattern matches in a function, the program _fails_ with a runtime error - this is deliberate!

```erlang
case f(...) of
    Pattern1 ->
        Expressions1;
    Pattern2 ->
        Expressions2;
    ...

    LastPattern ->
        LastExpression
end
```

- Notice there is _NO_ semicolon `;` after the last expression!

## Funs

- **Fun**: the data type that represents a function
- Funs are \*anonymous functions\*\*
  - similar to **lambda abstractions**
- module `lists` is in standard library
- `lists:map(F, L)`
- `lists:filter(P, L)`
- `=:=` tests for equality
- `lists:member(X, L)`
  - returns `true` if `X` is a member of list `L`

## Parallelizing Sequential Code

- `catch F(H)` vs. `F(H)`
  - `catch F(H)` makes sure the wrapping function terminates correctly if `F(H)` raises an exception
- There is _overhead_ setting up a process and waiting for a reply!

## Running SMP Erlang

- **Symmetric Multiprocessing (SMP)** machine
  - has two or more identical CPUs connected to a single shared memory

## List Comprehensions

- **List comprehensions** - expressions that create lists _without_ having to use funs, maps, or filters
- `[F(X) || X <- L]`
  - the list of `F(X)` where `X` is take from list `L`
- `map(F, L) -> [F(X) || X <- L].`
- the _most_ general form: `[X || Qualifier1, Qualifier2, ...]`
  - `X` is arbitrary expression
  - each qualifier is either a **generator**, a **bitstring generator**, or a **filter**
- **Generator** - `Pattern <- ListExpr`
- **Bitstring generator** - `BitStringPattern <= BitStringExpr`
  - where `BitStringExpr` must be an expression evaluates to a **bitstring**
- Filters are either:
  - **predicates**, or
  - boolean expressions
- `++` - the infix append operator
- `--` - list subtraction operator

## BIFs

- **Built-in Function**
- `list_to_tuple/1`
- `time/0`
- the most common BIFs are _autoimported_

## Guards

- \***\*Guards\*\*** are constructs that can be used to enhance pattern matching
- Can be used to perform simple tests and comparisons on the variables in a pattern
- Guards can be used in _two_ places:
  - in the heads of a function where they are introduced by `when`
  - at any place where an expression is allowed
    - when guards are used as _expressions_, they are evaluated to one of the atoms `true` or `false`
- **Guard sequences**
  - either a single or a series of guards, separated by `;`
  - is `true` if _at least one_ of them `G1, G2, ...` evaluates to `true`
    - basically an OR operation
- A **guard** is a series of guard expressions, separated by commas `,`
  - the guard `GuardExpr1, GuardExpr2, ..., GuardExprN` is `true` _only if_ all of them evaluate to `true`
- The set of valid guard expressions is a subset of all valid Erlang expressions
  - reason for the restriction: free from side effetcs
  - \***\*pattern matching has NO side effects\*\***
- Guards _CANNOT_ call user-defined function - make sure they are side effect free!
- `andalso` and `orelse` vs. `and` and `or`
  - `and/or` were originally designed to evaluate _both_ their arguments
  ```erlang
  Expr1 orelse Expr2 % Expr2 evaluated _only if_ Expr1 is false
  Expr1 andalso Expr2 % Expr2 evaluated _only if_ Expr1 is true
  ```
- The `true` guard
  - used as a "catch all" guard at the end of an `if` expression
  ```erlang
  if
    Guard -> Expressions;
    Guard -> Expressions;
    ...
    true -> Expressoins
  end
  ```

## `case` and `if` functions

### `case` Expressions

```erlang
case Expression of
  Pattern1 [when Guard1] -> Expr_seq1;
  Pattern2 [when Guard2] -> Expr_seq2;
  ...
end
```

### `if`

```erlang
if
  Guard1 ->
    Expr_seq1;
  Guard2 ->
    Expr_seq2;
  ...
end
```

- _AT LEAST ONE_ of the guards in the `if` expression must evaluate to `true` - exception raised otherwise
- Often the final guard in `if` is the _atom_ `true`
- In Erlang, _all_ expressions are supposed to have values

## Building lists in natural order

- The most efficient way to build a list is to add the elements to the _HEAD_ of an existing list
- To reverse a list, use `lists:reverse/1` - _HIGHLY OPTIMIZED_
-
