-module(ptests).

-export([tests/1, fib/1]).

-import(lists, [map/2]).
-import(map, [pmap/2]).

tests([N]) ->
    Nsched = list_to_integer(atom_to_list(N)),
    run_tests(1, Nsched).

run_tests(N, Nsched) ->
    case test(N) of
        stop ->
            init:stop();
        Val ->
            io:format("~p.~n", [{Nsched, Val}]),
            run_tests(N + 1, Nsched)
    end.

test(1) ->
    %% Make 100 lists
    %% Each list contains 1000 random integers
    seed(),
    S = lists:seq(1, 100),
    L = map(fun(_) -> makeList(1000) end, S),
    {Time1, S1} = timer:tc(lists, map, [fun lists:sort/1, L]),
    {Time2, S2} = timer:tc(map, pmap, [fun lists:sort/1, L]),
    {sort, Time1, Time2, equal(S1, S2)};
test(2) ->
    %% L = [27, 27, ...] 100 times
    L = lists:duplicate(100, 27),
    {Time1, S1} = timer:tc(lists, map, [fun ptests:fib/1, L]),
    {Time2, S2} = timer:tc(map, pmap, [fun ptests:fib/1, L]),
    {fib, Time1, Time2, equal(S1, S2)};
test(3) ->
    stop.

%% equal() tests that map and pmap compute the same thing
equal(S, S) ->
    true;
equal(S1, S2) ->
    {differ, S1, S2}.

%% recursive (and INEFFICIENT) Fibonacci
fib(0) ->
    1;
fib(1) ->
    1;
fib(N) ->
    fib(N - 1) + fib(N - 2).

%% Reset the random number generator
%% So that we get the same sequence of random numbers each time the program is run
seed() -> random:seed(44, 55, 66).

makeList(K) -> makeList(K, []).
makeList(0, L) -> L;
makeList(N, L) -> makeList(N - 1, [random:uniform(1000000) | L]).
