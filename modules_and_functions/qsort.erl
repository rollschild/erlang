-module(qsort).

-export([qsort/1]).

%% this implementation focuses more on elegancy than performance
qsort([]) ->
    [];
qsort([Pivot | T]) ->
    qsort([X || X <- T, X < Pivot]) ++ [Pivot] ++ qsort([X || X <- T, X >= Pivot]).
