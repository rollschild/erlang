-module(shop_total).

-export([total/1, total_p/1, total_short/1]).

-import(lists, [map/2, sum/1]).
-import(map, [pmap/2]).

% total([{What, N} | T]) ->
% shop:cost(What) * N + total(T);
% total([]) ->
% 0.

total(L) ->
    sum(map(fun({What, N}) -> shop:cost(What) * N end, L)).

total_short(L) ->
    sum([shop:cost(What) * N || {What, N} <- L]).

total_p(L) ->
    sum(pmap(fun({What, N}) -> shop:cost(What) * N end, L)).
